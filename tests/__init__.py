from pathlib import Path
import sys

project_root_path = Path(".").resolve()
sys.path.insert(0, project_root_path)
print("adding", project_root_path)