from examplecurves.main import _calculate_curve_by_cubic_spline
import pytest
import numpy as np


def test_raising_value_error():
    target_does_not_matter_here = None
    with pytest.raises(ValueError):
        multidimensional_points = np.arange(4 * 3 * 2).reshape(4, 3, 2)
        _calculate_curve_by_cubic_spline(
            base_points=multidimensional_points, target_x=target_does_not_matter_here
        )

    with pytest.raises(ValueError):
        _calculate_curve_by_cubic_spline(
            base_points=1, target_x=target_does_not_matter_here
        )
