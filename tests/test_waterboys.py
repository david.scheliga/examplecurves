from examplecurves.main import get_resource_path
import pytest


def test_get_resource_path():
    existing_resource_path = get_resource_path("linear_curves.json")
    assert existing_resource_path.name == "linear_curves.json"

    with pytest.raises(FileNotFoundError):
        get_resource_path("non_existing.file")