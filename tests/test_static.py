import examplecurves
import pytest


def test_raised_errors():
    with pytest.raises(ValueError):
        examplecurves.Static.create("non_existing_family")

    with pytest.raises(IndexError):
        examplecurves.Static.create(family_name="verticallinear0", predefined_offset=99)

    with pytest.raises(ValueError):
        examplecurves.Static.create(family_name="verticallinear0", curve_selection=[])