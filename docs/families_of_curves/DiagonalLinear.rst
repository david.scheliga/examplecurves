﻿Group of Diagonal Linear
========================

Each family of curves of the group *horizontal linear* is a standard normal
choice of 5 static linear curves. Their end points are *diagonally* aligned,
based on the assumption of an energy fail criterion. Current members are index
are indexed by **0 to 3**.

DiagonalLinear0
---------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("DiagonalLinear0")

DiagonalLinear1
---------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("DiagonalLinear1")

DiagonalLinear2
---------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("DiagonalLinear2")

DiagonalLinear3
---------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("DiagonalLinear3")