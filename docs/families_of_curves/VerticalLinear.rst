﻿Group of Vertical Linear
========================

Each family of curves of the group *vertical linear* is a standard normal
choice of 5 static linear curves. Their end points are *vertically* aligned,
based on the assumption of a strain fail criterion. Current members are index
are indexed by **0 to 3**.

VerticalLinear0
---------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("VerticalLinear0")

VerticalLinear1
---------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("VerticalLinear1")

VerticalLinear2
---------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("VerticalLinear2")

VerticalLinear3
---------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("VerticalLinear3")