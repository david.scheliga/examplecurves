*****************************
families of curves
*****************************

.. autosummary::
   :toctree:

    examplecurves.NonLinear0

.. toctree::

    HorizontalLinear
    DiagonalLinear
    VerticalLinear
