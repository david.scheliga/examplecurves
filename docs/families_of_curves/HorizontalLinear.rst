﻿Group of Horizontal Linear
==========================

Each family of curves of the group *horizontal linear* is a standard normal
choice of 5 static linear curves. Their end points are *horizontally* aligned,
based on the assumption of a strength fail criterion. Current members are index
are indexed by **0 to 3**.

HorizontalLinear0
-----------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("HorizontalLinear0")

HorizontalLinear1
-----------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("HorizontalLinear1")

HorizontalLinear2
-----------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("HorizontalLinear2")

HorizontalLinear3
-----------------

.. plot::

    from examplecurves import make_family_overview_plots
    make_family_overview_plots("HorizontalLinear3")